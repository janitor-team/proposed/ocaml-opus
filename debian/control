Source: ocaml-opus
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Kyle Robbertze <paddatrapper@debian.org>
Build-Depends: debhelper (>=12),
               debhelper-compat (=12),
               dh-ocaml (>= 1.1.0),
               libogg-ocaml-dev (>= 0.5.2),
               libopus-dev (>= 1.0.1),
               ocaml-findlib (>= 1.2.4),
               ocaml-nox,
               pkg-config
Standards-Version: 4.4.0
Homepage: http://savonet.sourceforge.net/
Vcs-Git: https://salsa.debian.org/ocaml-team/ocaml-opus.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocaml-opus

Package: libopus-ocaml
Architecture: any
Depends: ${misc:Depends}, ${ocaml:Depends}, ${shlibs:Depends}
Provides: ${ocaml:Provides}
Description: OCaml interface to the opus library -- runtime files
 This package provides an interface to the opus library for
 OCaml programmers.
 .
 The Opus codec is designed for interactive speech and audio transmission over
 the Internet. It is designed by the IETF Codec Working Group and incorporates
 technology from Skype's SILK codec and Xiph.Org's CELT codec.
 .
 This package contains only the shared runtime stub libraries.

Package: libopus-ocaml-dev
Architecture: any
Depends: libogg-ocaml-dev,
         libopus-dev,
         libopus-ocaml (= ${binary:Version}),
         ocaml-findlib,
         ${misc:Depends},
         ${ocaml:Depends}
Provides: ${ocaml:Provides}
Description: OCaml interface to the opus library -- development files
 This package provides an interface to the opus library for
 OCaml programmers.
 .
 The Opus codec is designed for interactive speech and audio transmission over
 the Internet. It is designed by the IETF Codec Working Group and incorporates
 technology from Skype's SILK codec and Xiph.Org's CELT codec.
 .
 This package contains all the development stuff you need to develop
 OCaml programs which use ocaml-opus.
